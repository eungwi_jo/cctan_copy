﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : Singleton<PopupManager>
{
    public enum POPUP_TYPE
    {
        IntroPopup,
        ContinuePopup,
        ResultPopup,
    }

    [SerializeField] private GameObject _popupRoot;

    private Dictionary<POPUP_TYPE, GameObject> _popupPrefabs = new Dictionary<POPUP_TYPE, GameObject>();
    private Popup _currentPopup;

    public void OpenPopup(PopupData popupData)
    {
        if (_currentPopup != null)
        {
            _currentPopup.ClosePopup();
        }

        _currentPopup = GetPopup(popupData.PopupType);
        _currentPopup.OpenPopup(popupData);
    }

    public void ClosePopup()
    {
        if (_currentPopup != null)
        {
            _currentPopup.ClosePopup();
            _currentPopup = null;
        }
    }
    private Popup GetPopup(POPUP_TYPE popupType)
    {
        if (_popupPrefabs.ContainsKey(popupType) == false)
        {
            _popupPrefabs.Add(popupType, Resources.Load<GameObject>(string.Format("Popups/{0}", popupType)));
        }

        return NGUITools.AddChild(_popupRoot, _popupPrefabs[popupType]).GetComponent<Popup>();
    }

}
