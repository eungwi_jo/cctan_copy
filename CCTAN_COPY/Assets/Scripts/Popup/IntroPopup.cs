﻿public class IntroPopup : Popup
{
    public void OnClickPlay()
    {
        PopupManager.Instance.ClosePopup();
        PlayManager.Instance.Begin();
    }
}