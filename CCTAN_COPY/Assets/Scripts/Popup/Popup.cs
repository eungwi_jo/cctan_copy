﻿using UnityEngine;

public abstract class Popup : MonoBehaviour
{
    private PopupData _popupData;

    public virtual void OpenPopup(PopupData popupData)
    {
        _popupData = popupData;

        gameObject.SetActive(true);

        if (_popupData.OpenCallback != null)
            _popupData.OpenCallback();
    }

    public virtual void ClosePopup()
    {
        gameObject.SetActive(false);

        if (_popupData.CloseCallback != null)
            _popupData.CloseCallback();
    }
}
