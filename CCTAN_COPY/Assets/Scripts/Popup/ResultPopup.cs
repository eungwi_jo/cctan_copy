﻿using UnityEngine;

public class ResultPopup : Popup
{
    [SerializeField] private UILabel _bestScoreLabel;
    [SerializeField] private UILabel _scoreLabel;

    private ResultPopupData _popupData;

    public override void OpenPopup(PopupData popupData)
    {
        _popupData = (ResultPopupData)popupData;

        _bestScoreLabel.text = _popupData.BestScore.ToString("N0");
        _scoreLabel.text = _popupData.Score.ToString("N0");

        base.OpenPopup(popupData);
    }

    public void OnClickPlay()
    {
        PopupManager.Instance.ClosePopup();
        PlayManager.Instance.Init();
    }
}
