﻿using UnityEngine;

public class ContinuePopup : Popup
{
    public void OnClickOneMore()
    {
        PopupManager.Instance.ClosePopup();
        UnityAds.Instance.ShowAdForOneMore();
    }

    public void OnClickEndGame()
    {
        PopupManager.Instance.ClosePopup();
        PlayManager.Instance.EndGame();
    }
}
