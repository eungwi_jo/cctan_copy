﻿using System;
using POPUP_TYPE = PopupManager.POPUP_TYPE;

public class PopupData
{
    public virtual POPUP_TYPE PopupType { get; set; }
    public Action OpenCallback;
    public Action CloseCallback;
}

public class IntroPopupData : PopupData
{
    public override POPUP_TYPE PopupType { get { return POPUP_TYPE.IntroPopup; } }
}

public class ResultPopupData : PopupData
{
    public override POPUP_TYPE PopupType { get { return POPUP_TYPE.ResultPopup; } }
    public int BestScore;
    public int Score;
}



