﻿using UnityEngine;

public class JPlayerPrefs
{
    private static readonly string _password = "cctan_copy!@#";

    public static void SetString(string key, string value)
    {
        PlayerPrefs.SetString(JEncDec.Encrypt(key, _password), JEncDec.Encrypt(value, _password));
    }

    public static string GetString(string key, string defaultValue = "")
    {
        key = JEncDec.Encrypt(key, _password);
        return PlayerPrefs.HasKey(key)? JEncDec.Decrypt(PlayerPrefs.GetString(key), _password) : defaultValue;
    }

    public static void SetInt(string key, int value)
    {
        PlayerPrefs.SetString(JEncDec.Encrypt(key, _password), JEncDec.Encrypt(value.ToString(), _password));
    }

    public static int GetInt(string key, int defaultValue = 0)
    {
        key = JEncDec.Encrypt(key, _password);
        return PlayerPrefs.HasKey(key)? JEncDec.Decrypt(PlayerPrefs.GetString(key), _password).ToInt() : defaultValue;
    }

    public static bool HasKey(string key)
    {
        key = JEncDec.Encrypt(key, _password);
        return PlayerPrefs.HasKey(key);
    }

    public static void DeleteKey(string key)
    {
        key = JEncDec.Encrypt(key, _password);
        if (PlayerPrefs.HasKey(key))
        {
            PlayerPrefs.DeleteKey(key);
        }
    }
}
