﻿using UnityEngine;

public class JMath
{
    public static int RandomSign()
    {
        return Random.Range(0, 2) == 0 ? -1 : 1;
    }

    public static Vector2 RandomBetween(Vector2 min, Vector2 max)
    {
        var area = Random.Range(0, 2);
        var result = Vector2.zero;

        switch (area)
        {
            case 0:
                result.x = Random.Range(-max.x, max.x);
                result.y = RandomSign() * Random.Range(min.y, max.y);
                break;
            case 1:
                result.x = RandomSign() * Random.Range(min.x, max.x);
                result.y = Random.Range(-max.y, max.y);
                break;
        }

        return result;
    }

    public static Vector3 Center(params Vector3[] vectors)
    {
        var sum = Vector3.zero;
        if (vectors == null || vectors.Length == 0)
            return sum;

        foreach (var vector in vectors)
        {
            sum += vector;
        }

        return sum / vectors.Length;
    }

    public static float GetDegree(Vector2 a, Vector2 b)
    {
        return (Mathf.Atan2(b.y, b.x) - Mathf.Atan2(a.y, a.x)) * Mathf.Rad2Deg;
    }

    public static Vector2 RotateVector2(Vector2 point, float degree)
    {
        var rad = degree * Mathf.Deg2Rad;
        var sin = Mathf.Sin(rad);
        var cos = Mathf.Cos(rad);

        return new Vector2(point.x * cos - point.y * sin, point.y * cos + point.x * sin);
    }
}
