﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public class JEncDec
{
    private static Dictionary<string, JEncDec> _instances = new Dictionary<string, JEncDec>();
    private static JEncDec GetInstance(string password)
    {
        if (_instances.ContainsKey(password) == false)
        {
            var manager = new JEncDec();
            manager.Init(password);
            _instances.Add(password, manager);
            return manager;
        }
        return _instances[password];
    }

    private Rijndael _alg;
    private void Init(string password)
    {
        PasswordDeriveBytes pdb = new PasswordDeriveBytes(password,
            new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,
                0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
        _alg = Rijndael.Create();
        _alg.Key = pdb.GetBytes(32);
        _alg.IV = pdb.GetBytes(16);
    }
    private byte[] Encrypt(byte[] clearData)
    {
        return _alg.CreateEncryptor().TransformFinalBlock(clearData, 0, clearData.Length);
    }
    private byte[] Decrypt(byte[] cipherData)
    {
        return _alg.CreateDecryptor().TransformFinalBlock(cipherData, 0, cipherData.Length);
    }

    public static byte[] Encrypt(byte[] clearData, string password)
    {
        return GetInstance(password).Encrypt(clearData);
    }
    public static byte[] Decrypt(byte[] cipherData, string password)
    {
        return GetInstance(password).Decrypt(cipherData);
    }
    public static string Encrypt(string text, string password)
    {
        var bs = Encrypt(Encoding.UTF8.GetBytes(text), password);
        return Convert.ToBase64String(bs);
    }
    public static string Decrypt(string text, string password)
    {
        var bs = Decrypt(Convert.FromBase64String(text), password);
        return Encoding.UTF8.GetString(bs);
    }

    public static void Encrypt(string fileIn, string fileOut, string password)
    {
        File.WriteAllBytes(fileOut, Encrypt(File.ReadAllBytes(fileIn), password));
    }


}
