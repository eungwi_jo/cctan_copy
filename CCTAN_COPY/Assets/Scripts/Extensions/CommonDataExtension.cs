﻿using System;
using System.Xml.Linq;

public static class CommonDataExtension
{
    public static int ToInt(this string text)
    {
        var result = 0;
        int.TryParse(text, out result);
        return result;
    }

    #region XElement

    public static int GetInt(this XElement element, string name, int defaultValue = 0)
    {
        var xAttribute = element.Attribute(name);
        return xAttribute != null ? int.Parse(xAttribute.Value) : defaultValue;
    }

    public static float GetFloat(this XElement element, string name, float defaultValue = 0)
    {
        var xAttribute = element.Attribute(name);
        return xAttribute != null ? float.Parse(xAttribute.Value) : defaultValue;
    }

    public static string GetString(this XElement element, string name, string defaultValue = "")
    {
        var xAttribute = element.Attribute(name);
        return xAttribute != null ? xAttribute.Value : defaultValue;
    }

    public static T GetEnum<T>(this XElement element, string name, T defaultValue = default(T))
    {
        var xAttribute = element.Attribute(name);
        return xAttribute != null ? (T)Enum.Parse(typeof(T), xAttribute.Value, true) : defaultValue;
    }

    #endregion
}
