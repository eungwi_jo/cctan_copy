﻿using System.Collections.Generic;
using UnityEngine;
using POOL_TYPE = ObjectPool.POOL_TYPE;

public class ObjectPools : Singleton<ObjectPools>
{
    [SerializeField] private List<ObjectPool> _objectPools = new List<ObjectPool>();

    void Awake()
    {
        for (var i = 0; i < _objectPools.Count; i++)
        {
            _objectPools[i].InitPool();
        }
    }

    public GameObject PopItem(POOL_TYPE poolType)
    {
        var objectPool = GetObjectPool(poolType);
        if (objectPool == null)
            return null;

        return objectPool.PopItem();
    }

    public void PushItem(POOL_TYPE poolType, GameObject item)
    {
        var objectPool = GetObjectPool(poolType);
        if (objectPool == null)
            return;

        objectPool.PushItem(item);
    }

    private ObjectPool GetObjectPool(POOL_TYPE poolType)
    {
        for (var i = 0; i < _objectPools.Count; i++)
        {
            if (_objectPools[i].PoolType.Equals(poolType))
                return _objectPools[i];
        }

        return null;
    }
}
