﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

[Serializable]
public class ObjectPool
{
    public enum POOL_TYPE
    {
        BRICK,
        ITEM,
        BRICK_HUD_UI,
        BULLET,
        BOMB,
        ITEM_BUFF
    }

    [SerializeField] private POOL_TYPE _poolType;
    [SerializeField] private GameObject _prefab;
    [SerializeField] private Transform _root;
    [SerializeField] private int _initPoolSize;

    protected List<GameObject> _pools;

    public POOL_TYPE PoolType { get { return _poolType; } }

    public void InitPool()
    {
        _pools = new List<GameObject>();

        for (var i = 0; i < _initPoolSize; i++)
        {
            _pools.Add(CreateItem());

        }
    }

    private GameObject CreateItem()
    {
        var item = Object.Instantiate(_prefab, _root);
        item.SetActive(false);

        return item;
    }

    public GameObject PopItem()
    {
        if (_pools.Count == 0)
        {
            _pools.Add(CreateItem());
        }

        var item = _pools[0];
        _pools.RemoveAt(0);

        return item;
    }

    public void PushItem(GameObject item)
    {
        item.SetActive(false);
        _pools.Add(item);
    }
}
