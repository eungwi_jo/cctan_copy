﻿public interface IPlayPublisher
{
    void AddObserver(IPlayObserver observer);
    void RemoveObserver(IPlayObserver observer);
    bool ContainObserver(IPlayObserver observer);
    void NotifyState(PlayManager.PLAY_STATE playState);
} 

public interface ILevelPublisher
{
    void AddObserver(ILevelObserver observer);
    void RemoveObserver(ILevelObserver observer);
    bool ContainObserver(ILevelObserver observer);
    void NotifyLevel(int level);
}