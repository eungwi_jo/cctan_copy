﻿public interface IPlayObserver
{
    void OnNotify(PlayManager.PLAY_STATE playState);
}

public interface ILevelObserver
{
    void OnNotify(int level);
}