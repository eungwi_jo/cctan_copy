﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ColorBlink : MonoBehaviour
{
    [SerializeField] private Color[] _colors = new Color[] { Color.white };
    [SerializeField] private Color _lastColor = Color.white;

    [SerializeField] private float[] _timings;
    [SerializeField] private bool _usingLerp;
    [SerializeField] private bool _autoPlay;
    [SerializeField] private bool _isLoop;

    private int _index;
    private float _timing;
    private float _elapsedTime;

    void Awake()
    {
        if (_autoPlay)
        {
            Play();
        }
        else
        {
            enabled = false;
        }
    }

    void OnDisable()
    {
        enabled = false;
    }

    void Update()
    {
        if (CheckTiming())
        {
            _elapsedTime = 0;
            _index++;
            _timing = _index < _timings.Length ? _timings[_index] : _timings[_timings.Length - 1];

            if (_index >= _colors.Length)
            {
                if (_isLoop)
                {
                    _index = 0;
                }
                else
                {
                    End();
                    return;
                }
            }

            PlayBlinkColor();
        }
        else if (_usingLerp)
        {
            if (_index >= _colors.Length)
            {
                if (_isLoop)
                {
                    _index = 0;
                }
                else
                {
                    End();
                    return;
                }
            }

            PlayBlinkColorLerp();
        }
    }

    public void Play(Color lastColor)
    {
        _lastColor = lastColor;
        Play();
    }

    public void Play()
    {
        _index = 0;
        _timing = _index < _timings.Length ? _timings[_index] : _timings[_timings.Length - 1];
        _elapsedTime = 0;

        PlayBlinkColor();
        enabled = true;
    }

    private void End()
    {
        enabled = false;
        SetColor(_lastColor);
    }

    private bool CheckTiming()
    {
        _elapsedTime += Time.smoothDeltaTime;
        return _timing == 0 || _elapsedTime >= _timing;
    }

    private void PlayBlinkColor()
    {
        SetColor(_colors[_index]);
    }

    private void PlayBlinkColorLerp()
    {
        var from = _colors[_index];
        var to = _index < _colors.Length - 1 ? _colors[_index + 1] : _lastColor;
        SetColor(Colorx.Slerp(from, to, _elapsedTime / _timing));
    }

    protected abstract void SetColor(Color color);
}
