﻿using UnityEngine;

public class UIWidgetColorBlink : ColorBlink
{
    [SerializeField] private UIWidget[] _widgets;

    protected override void SetColor(Color color)
    {
        for (var i = 0; i < _widgets.Length; i++)
        {
            _widgets[i].color = color;
        }
    }
}
