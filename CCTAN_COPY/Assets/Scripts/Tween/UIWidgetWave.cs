﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class UIWidgetWave : MonoBehaviour
{
    public enum DEPTH_CONTROL_TYPE
    {
        ABSOLUTE,
        RELATIVE,
    }
    public GameObject Root;
    public UIWidget TargetWidget;

    [Space]
    public bool IsAutoPlay = true;
    public float Delay = 0;
    public float Frequency = .5f;
    public float Duration = 0;
    public bool IgnoreTimeScale = false;

    [Space]
    public bool EnableIfDisable = true;
    public DEPTH_CONTROL_TYPE DepthControlType = DEPTH_CONTROL_TYPE.RELATIVE;
    public int Depth = -1;

    [HideInInspector] public bool UseTweenColor = false;
    [HideInInspector] public float ColorTime = .5f;
    [HideInInspector] public Color FromColor = Color.white;
    [HideInInspector] public Color ToColor = new Color(1, 1, 1, 0);
    [HideInInspector] public AnimationCurve ColorCurve = AnimationCurve.Linear(0, 0, 1, 1);
    [HideInInspector] public bool DisableAfterColor = true;

    [HideInInspector] public bool UseTweenScale = false;
    [HideInInspector] public float ScaleTime = .5f;
    [HideInInspector] public Vector3 FromScale = Vector3.one;
    [HideInInspector] public Vector3 ToScale = Vector3.one * 1.5f;
    [HideInInspector] public AnimationCurve ScaleCurve = AnimationCurve.Linear(0, 0, 1, 1);
    [HideInInspector] public bool DisableAfterScale = true;

    [HideInInspector] public bool UseTweenPos = false;
    [HideInInspector] public float PosTime = .5f;
    [HideInInspector] public Vector3 FromPos = Vector3.zero;
    [HideInInspector] public Vector3 ToPos = Vector3.zero;
    [HideInInspector] public AnimationCurve PosCurve = AnimationCurve.Linear(0, 0, 1, 1);
    [HideInInspector] public bool DisableAfterPos = true;

    private float _delayCheck;
    private float _frequencyCheck;
    private float _durationCheck;

    private bool _isPlaying;
    private Action _callback;

    private readonly List<UIWidget> _cachedWaves = new List<UIWidget>();

    #region Unity

    void Start()
    {
        Init();

        if (IsAutoPlay)
        {
            PlayEffect(false);
        }
    }

    void Update()
    {
        if (!IsPlaying())
        {
            EndEffect();
            return;
        }

        var smoothDeltaTime = IgnoreTimeScale ? Time.unscaledDeltaTime : Time.smoothDeltaTime;

        _delayCheck += smoothDeltaTime;
        if (_delayCheck > 0 && _delayCheck < Delay)
            return;


        _durationCheck += smoothDeltaTime;
        _frequencyCheck += smoothDeltaTime;

        if (Frequency > 0 && _frequencyCheck >= Frequency)
        {
            _frequencyCheck = 0;
            MakeWave();
        }
    }

    #endregion

    #region Play

    private void Init()
    {
        ResetEffect();
    }

    private bool IsPlaying()
    {
        if (!_isPlaying)
            return false;

        if (Duration > 0 && _durationCheck > Duration)
        {
            return false;
        }

        return true;
    }

    public void Emit(int count)
    {
        if (!UseTweenColor && !UseTweenScale && !UseTweenPos)
        {
            Debug.LogWarning("At least one Tween setting is required.");
            return;
        }

        for (var i = 0; i < count; i++)
        {
            MakeWave();
        }
    }

    public void PlayEffect(bool ignoreDelay, Action callback = null)
    {
        if (!UseTweenColor && !UseTweenScale && !UseTweenPos)
        {
            Debug.LogWarning("At least one Tween setting is required.");
            return;
        }

        _callback = callback;
        _delayCheck = ignoreDelay ? _delayCheck : 0;
        _durationCheck = 0;
        _frequencyCheck = Frequency;

        _isPlaying = true;
        enabled = true;
    }

    private void EndEffect()
    {
        _isPlaying = false;
        enabled = false;

        ResetEffect();

        if (_callback != null)
            _callback();

    }

    private void ResetEffect()
    {
        DestroyAllWaves();
    }

    #endregion

    #region Wave

    private void MakeWave()
    {
        var wave = GetWave();

        if (EnableIfDisable && !wave.gameObject.activeSelf)
            wave.gameObject.SetActive(true);

        if (TargetWidget is UILabel)
        {
            ((UILabel)wave).text = ((UILabel)TargetWidget).text;
        }

        switch (DepthControlType)
        {
            case DEPTH_CONTROL_TYPE.ABSOLUTE:
                wave.depth = Depth;
                break;
            case DEPTH_CONTROL_TYPE.RELATIVE:
                wave.depth = TargetWidget.depth + Depth;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        if (UseTweenColor)
        {
            wave.color = FromColor;
            DOTween.To(t => wave.color = Color.Lerp(FromColor, ToColor, t), 0, 1f, ColorTime).OnComplete(
                delegate
                {
                    if (DisableAfterColor) wave.gameObject.SetActive(false);

                });
        }

        if (UseTweenScale)
        {
            wave.transform.localScale = FromScale;
            wave.transform.DOScale(ToScale, ScaleTime).SetEase(ScaleCurve).OnComplete(
                delegate
                {
                    if (DisableAfterScale) wave.gameObject.SetActive(false);
                });
        }

        if (UseTweenPos)
        {
            wave.transform.localPosition = FromPos;
            wave.transform.DOLocalMove(ToPos, PosTime).SetEase(PosCurve).OnComplete(
                delegate
                {
                    if (DisableAfterPos) wave.gameObject.SetActive(false);
                });
        }
    }

    private UIWidget GetWave()
    {
        for (var i = 0; i < _cachedWaves.Count; i++)
        {
            if (DOTween.IsTweening(_cachedWaves[i].transform))
                continue;

            return _cachedWaves[i];
        }


        var wave = Root.AddChild(TargetWidget.gameObject).GetComponent<UIWidget>();
        _cachedWaves.Add(wave);

        return wave;
    }

    private void DestroyAllWaves()
    {
        UIWidget wave;

        for (var i = _cachedWaves.Count - 1; i >= 0; i--)
        {
            wave = _cachedWaves[i];
            _cachedWaves.Remove(wave);

            Destroy(wave.gameObject);
        }

        _cachedWaves.Clear();
    }
    #endregion
}
