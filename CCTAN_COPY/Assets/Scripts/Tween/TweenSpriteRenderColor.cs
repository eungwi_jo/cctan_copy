﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Style = UITweener.Style;

public class TweenSpriteRenderColor : MonoBehaviour
{
    [SerializeField] private Color[] _colors;
    [SerializeField] private SpriteRenderer _target;
    [SerializeField] private Style _style;
    [SerializeField] private int _maxLoop;
    [SerializeField] private bool _autoPlay;

    private int _index;
    private int _colorCount;
    private bool _isPlayForward;
    private int _loopCount;

    void Start()
    {
        _colorCount = _colors.Length;

        if (_autoPlay)
            PlayTween();
    }

    void Update()
    {
        switch (_style)
        {
            case Style.Loop:
                _isPlayForward = true;
                _index++;
                if (_index >= _colorCount)
                {
                    _index = 0;
                    if (_maxLoop > 0)
                    {
                        _loopCount++;
                        if (_loopCount >= _maxLoop)
                        {
                            Finish();
                            return;
                        }
                    }
                }
                break;

            case Style.PingPong:
                _index = _isPlayForward ? _index + 1 : _index - 1;
                if (_index >= _colorCount)
                {
                    _isPlayForward = false;
                    _index = _colorCount - 2;
                    if (_maxLoop > 0)
                    {
                        _loopCount++;
                        if (_loopCount >= _maxLoop)
                        {
                            Finish();
                            return;
                        }
                    }

                }
                else if (_index < 0)
                {
                    _isPlayForward = true;
                    _index = 1;
                    if (_maxLoop > 0)
                    {
                        _loopCount++;
                        if (_loopCount >= _maxLoop)
                        {
                            Finish();
                            return;
                        }
                    }
                }

                break;

            case Style.Once:
                _isPlayForward = true;
                _index++;
                if (_index >= _colorCount)
                {
                    Finish();
                    return;
                }

                break;
        }

        PlayTween();
    }

    private void Finish()
    {
    }

    private void PlayTween()
    {
    }
}
