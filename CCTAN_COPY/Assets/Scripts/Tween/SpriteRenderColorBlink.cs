﻿using UnityEngine;

public class SpriteRenderColorBlink : ColorBlink
{
    [SerializeField] private SpriteRenderer[] _spriteRenderers;

    protected override void SetColor(Color color)
    {
        for (var i = 0; i < _spriteRenderers.Length; i++)
        {
            _spriteRenderers[i].color = color;
        }
    }
}
