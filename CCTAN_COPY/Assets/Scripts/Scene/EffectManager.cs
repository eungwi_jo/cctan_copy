﻿using System.Collections.Generic;
using UnityEngine;
using POOL_TYPE = ObjectPool.POOL_TYPE;

public class EffectManager : Singleton<EffectManager>
{
    [SerializeField] private ParticleSystem _brickDestroyEffect;

    private List<GameObject> _bombs = new List<GameObject>();

    public void PlayBrickDestroyEffect(Vector3 pos)
    {
        _brickDestroyEffect.transform.position = pos;
        _brickDestroyEffect.Play();
    }

    public GameObject PlayBombEffect(Vector3 pos)
    {
        var bomb = ObjectPools.Instance.PopItem(POOL_TYPE.BOMB);
        bomb.GetComponent<Bomb>().Play(pos);
        _bombs.Add(bomb);

        return bomb;
    }

    public void DestoryBombEffect(GameObject bomb)
    {
        _bombs.Remove(bomb);

        ObjectPools.Instance.PushItem(POOL_TYPE.BOMB, bomb);
    }
}
