﻿using DG.Tweening;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private GameObject _head;
    private Sequence _headTweenSequence;

    void Start()
    {
        PlayHeadTween();
    }

    void PlayHeadTween()
    {
        if (_headTweenSequence != null && _headTweenSequence.IsPlaying())
            _headTweenSequence.Kill();

        _head.transform.rotation = Quaternion.Euler(Vector3.zero);

        _headTweenSequence = DOTween.Sequence();
        _headTweenSequence.Append(_head.transform.DOLocalRotate(new Vector3(0, 0, 45), 1f).SetEase(Ease.OutExpo).SetDelay(1.5f))
            .Append(_head.transform.DOLocalRotate(new Vector3(0, 0, 0), 1f).SetEase(Ease.OutExpo).SetDelay(.5f))
            .Append(_head.transform.DOLocalRotate(new Vector3(0, 0, -45), 1f).SetEase(Ease.OutExpo).SetDelay(1.5f))
            .Append(_head.transform.DOLocalRotate(new Vector3(0, 0, 0), 1f).SetEase(Ease.OutExpo).SetDelay(.5f))
            .Append(_head.transform.DOLocalRotate(new Vector3(0, 0, -360), .8f, RotateMode.FastBeyond360).SetDelay(1.5f))
            .SetLoops(-1);
    }
}
