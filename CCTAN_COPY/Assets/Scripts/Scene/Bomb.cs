﻿using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] _particles;

    public void Play(Vector3 position)
    {
        gameObject.SetActive(true);
        gameObject.transform.position = position;

        for (var i = 0; i < _particles.Length; i++)
        {
            _particles[i].Play();
        }
    }
}
