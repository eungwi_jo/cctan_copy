﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ITEM_TYPE = ItemData.ITEM_TYPE;

public class ItemManager : Singleton<ItemManager>, IPlayObserver
{
    private List<ItemInstance> _itemInstances = new List<ItemInstance>();

    void Awake()
    {
        PlayManager.Instance.PlayUpdate += ItemUpdate;
    }

    private void Init()
    {
        Clear();
    }

    private void Clear()
    {
        for (var i = _itemInstances.Count - 1; i >= 0; i--)
        {
            RemoveItemInstance(_itemInstances[i]);
        }
    }

    void ItemUpdate()
    {
        for (var i = _itemInstances.Count - 1; i >= 0; i--)
        {
            var itemInstance = _itemInstances[i];
            itemInstance.Update();

            if (itemInstance.RemainTime <= 0)
            {
                RemoveItemInstance(itemInstance);
            }
        }
    }

    public void GetItem(ItemData itemData)
    {
        var itemInstance = GetItemInstance(itemData.ItemType);
        if (itemInstance != null)
        {
            itemInstance.RemainTime += itemData.Duration;
        }
        else
        {
            AddItemInstance(new ItemInstance(itemData));
        }
    }

    private ItemInstance GetItemInstance(ITEM_TYPE itemType)
    {
        if (itemType == ITEM_TYPE.BOMB)
            return null;

        for (var i = 0; i < _itemInstances.Count; i++)
        {
            if (itemType == _itemInstances[i].ItemData.ItemType)
                return _itemInstances[i];
        }

        return null;
    }

    private void AddItemInstance(ItemInstance itemInstance)
    {
        itemInstance.OnStart();

        if (itemInstance.ItemData.Duration > 0)
            _itemInstances.Add(itemInstance);
    }

    private void RemoveItemInstance(ItemInstance itemInstance)
    {
        itemInstance.OnEnd();
        _itemInstances.Remove(itemInstance);
    }

    public void OnNotify(PlayManager.PLAY_STATE playState)
    {
        switch (playState)
        {
            case PlayManager.PLAY_STATE.IDLE:
                Init();
                break;
            case PlayManager.PLAY_STATE.BEGIN:
                break;
            case PlayManager.PLAY_STATE.PLAYING:
                break;
            case PlayManager.PLAY_STATE.END:
                Clear();
                break;
            default:
                throw new ArgumentOutOfRangeException("playState", playState, null);
        }
    }


    public class ItemInstance
    {
        public ItemData ItemData;
        public float RemainTime;

        private GameObject _effect;
        private ItemBuff _itemBuff;
        public ItemInstance(ItemData itemData)
        {
            ItemData = itemData;
            RemainTime = itemData.Duration;
        }

        public void Update()
        {
            RemainTime -= Time.smoothDeltaTime;

            if (_itemBuff != null)
                _itemBuff.UpdateUI();
        }

        public void OnStart()
        {
            switch (ItemData.ItemType)
            {
                case ITEM_TYPE.LEVEL_UP:
                    PlayManager.Instance.LevelUp();
                    break;
                case ITEM_TYPE.LAZER:
                    _itemBuff = LauncherUI.Instance.AddItemBuff(this);
                    LauncherManager.Instance.SetLazer();
                    break;
                case ITEM_TYPE.BACK:
                    _itemBuff = LauncherUI.Instance.AddItemBuff(this);
                    LauncherManager.Instance.SetBack();
                    break;
                case ITEM_TYPE.STOP:
                    _itemBuff = LauncherUI.Instance.AddItemBuff(this);
                    BrickSpawner.Instance.Pause();
                    break;
                case ITEM_TYPE.BOMB:
                    _effect = EffectManager.Instance.PlayBombEffect(ItemData.Pos);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void OnEnd()
        {
            switch (ItemData.ItemType)
            {
                case ITEM_TYPE.LEVEL_UP:
                    break;
                case ITEM_TYPE.LAZER:
                    LauncherUI.Instance.RemoveItemBuff(_itemBuff);
                    LauncherManager.Instance.UnsetLazer();
                    break;
                case ITEM_TYPE.BACK:
                    LauncherUI.Instance.RemoveItemBuff(_itemBuff);
                    LauncherManager.Instance.UnsetBack();
                    break;
                case ITEM_TYPE.STOP:
                    LauncherUI.Instance.RemoveItemBuff(_itemBuff);
                    BrickSpawner.Instance.Resume();
                    break;
                case ITEM_TYPE.BOMB:
                    EffectManager.Instance.DestoryBombEffect(_effect);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

}
