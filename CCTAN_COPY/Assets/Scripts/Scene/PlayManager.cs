﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayManager : Singleton<PlayManager>, IPlayPublisher, ILevelPublisher
{
    public enum PLAY_STATE
    {
        IDLE,
        BEGIN,
        PLAYING,
        END,
    }

    [SerializeField] private TopUI _topUI;

    private List<IPlayObserver> _playObservers = new List<IPlayObserver>();
    private List<ILevelObserver> _levelObservers = new List<ILevelObserver>();

    public Action PlayUpdate;
    public float PlayTime { get; private set; }

    private int _score;
    private int _bestScore;

    private int _level;
    public int Level
    {
        get { return _level; }
        private set
        {
            _level = value;
            NotifyLevel(_level);
        }
    }

    private PLAY_STATE _playState;
    public PLAY_STATE PlayState
    {
        get { return _playState; }
        private set
        {
            _playState = value;
            NotifyState(_playState);
        }
    }

    public bool IsFirstGame { get; private set; }

    #region Unity

    void Start()
    {
        Init();
    }

    void Update()
    {
        if (PlayState == PLAY_STATE.PLAYING)
        {
            PlayTime = PlayTime + Time.smoothDeltaTime;
            PlayUpdate();
        }
    }

    #endregion

    public void LevelUp()
    {
        Level++;
    }

    #region GameState

    public void Init()
    {
        PlayState = PLAY_STATE.IDLE;

        Level = 1;

        _score = 0;
        _topUI.SetScore(_score);

        _bestScore = JPlayerPrefs.GetInt("BestScore");
        _topUI.SetBestScore(_bestScore);

        OpenIntroPopup();
    }

    public void Begin()
    {
        //Init
        PlayState = PLAY_STATE.BEGIN;
        Level = 1;
        PlayTime = 0;
        Time.timeScale = 1f;
        IsFirstGame = true;

        Play();
    }

    private void Play()
    {
        Time.timeScale = 1;
        PlayState = PLAY_STATE.PLAYING;
    }

    public void Die()
    {
        if (PlayState == PLAY_STATE.END)
            return;

        Time.timeScale = 0;
        PlayState = PLAY_STATE.END;

        if (IsFirstGame)
        {
            var popupData = new PopupData { PopupType = PopupManager.POPUP_TYPE.ContinuePopup };
            PopupManager.Instance.OpenPopup(popupData);
        }
        else
        {
            EndGame();
        }
    }

    public void OneMoreGame()
    {
        IsFirstGame = false;
        Play();
    }

    public void EndGame()
    {
        if (_score > _bestScore)
        {
            JPlayerPrefs.SetInt("BestScore", _score);
            _bestScore = _score;
        }

        OpenResultPopup();
    }

    public void AddScore()
    {
        _score++;
        _topUI.SetScore(_score);
    }

    private void OpenIntroPopup()
    {
        var popupData = new IntroPopupData();
        PopupManager.Instance.OpenPopup(popupData);

    }

    private void OpenResultPopup()
    {
        var popupData = new ResultPopupData();
        popupData.BestScore = _bestScore;
        popupData.Score = _score;

        PopupManager.Instance.OpenPopup(popupData);
    }

    #endregion

    #region IPlayPublisher

    public void AddObserver(IPlayObserver observer)
    {
        if (ContainObserver(observer))
            return;

        _playObservers.Add(observer);
    }

    public void RemoveObserver(IPlayObserver observer)
    {
        if (ContainObserver(observer) == false)
            return;

        _playObservers.Remove(observer);
    }

    public bool ContainObserver(IPlayObserver observer)
    {
        return _playObservers.Contains(observer);
    }

    public void NotifyState(PLAY_STATE playState)
    {
        for (var i = 0; i < _playObservers.Count; i++)
        {
            _playObservers[i].OnNotify(playState);
        }
    }

    #endregion

    #region ILevelPublisher

    public void AddObserver(ILevelObserver observer)
    {

        if (ContainObserver(observer))
            return;

        _levelObservers.Add(observer);
    }

    public void RemoveObserver(ILevelObserver observer)
    {
        if (ContainObserver(observer) == false)
            return;

        _levelObservers.Remove(observer);
    }

    public bool ContainObserver(ILevelObserver observer)
    {
        return _levelObservers.Contains(observer);
    }

    public void NotifyLevel(int level)
    {
        for (var i = 0; i < _levelObservers.Count; i++)
        {
            _levelObservers[i].OnNotify(level);
        }
    }

    #endregion
}
