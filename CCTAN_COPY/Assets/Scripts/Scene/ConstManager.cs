﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

public enum CONST_TYPE
{
    FRAME_RATE,
    LAZER_DISTANCE,
    LAZER_RADIUS,
    LAZER_COOLDOWN,
    BULLET_SPEED,
    BULLET_COOLDOWN,
    ITEM_SPAWN_RATE,
    BRICK_SPAWN_RATE,
    BRICK_SPEED,
    COMBINE_MINIMUM_DISTANCE,
    CONTINUE_MAXIMUM_DISTANCE,
    SQURE_POINTS,
    TRIANGLE_POINTS,
}

public class ConstManager : Singleton<ConstManager>
{
    private Dictionary<CONST_TYPE, float> _consts = new Dictionary<CONST_TYPE, float>();

    void Awake()
    {
        Init();

        Application.targetFrameRate = (int)GetConst(CONST_TYPE.FRAME_RATE);
    }

    private void Init()
    {
        var textAsset = Resources.Load<TextAsset>("GameSetting");
        var xDoc = XDocument.Parse(textAsset.text);
        var root = xDoc.Element("GameSetting");

        foreach (var element in root.Elements())
        {
            var key = element.GetEnum<CONST_TYPE>("Key");
            var value = element.GetFloat("Value");
            _consts.Add(key, value);
        }
    }

    public float GetConst(CONST_TYPE constType)
    {
        return _consts.ContainsKey(constType) ? _consts[constType] : 0;
    }
}
