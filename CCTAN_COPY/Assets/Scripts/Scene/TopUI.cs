﻿using UnityEngine;

public class TopUI : MonoBehaviour
{
    [SerializeField] private UIWidgetWave _scoreWave;
    [SerializeField] private UILabel _scoreLabel;
    [SerializeField] private UILabel _bestScoreLabel;

    public void SetBestScore(int score)
    {
        _bestScoreLabel.text = score.ToString("N0");
    }

    public void SetScore(int score)
    {
        _scoreLabel.text = score.ToString("N0");
        _scoreWave.Emit(1);
    }

    public void OnClickPause()
    {
        PlayManager.Instance.Init();
    }
}
