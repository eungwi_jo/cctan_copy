﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : Singleton<ResourceManager>
{
    public enum SPRITE_TYPE
    {
        BULLET,
        BRICK,
        ITEM_ICON,
    }

    private Dictionary<string, Sprite> _cachedSprites = new Dictionary<string, Sprite>();

    public Sprite GetSprite(SPRITE_TYPE spriteType, string spriteName)
    {
        var path = GetPath(spriteType, spriteName);
        if (_cachedSprites.ContainsKey(path) == false)
            _cachedSprites.Add(path, Resources.Load<Sprite>(path));

        return _cachedSprites[path];
    }

    private string GetPath(SPRITE_TYPE spriteType, string textureName)
    {
        var path = "Textures/";

        switch (spriteType)
        {
            case SPRITE_TYPE.BULLET:
                path += "Bullets/";
                break;
            case SPRITE_TYPE.BRICK:
                break;
            case SPRITE_TYPE.ITEM_ICON:
                path += "ItemIcons/";
                break;
        }

        path += textureName;
        return path;
    }
}
