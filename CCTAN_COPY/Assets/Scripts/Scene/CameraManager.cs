﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : Singleton<CameraManager>
{
    public enum CAMERA_TYPE
    {
        MAIN,
        UI
    }

    [SerializeField] private List<CameraData> _cameras;
    private Dictionary<CAMERA_TYPE, CameraData> _cachedCameras;

    void Awake()
    {
        _cachedCameras = new Dictionary<CAMERA_TYPE, CameraData>();

        for (var i = 0; i < _cameras.Count; i++)
        {
            _cameras[i].Bounds = _cameras[i].Camera.OrthographicBounds();
            _cachedCameras.Add(_cameras[i].CameraType, _cameras[i]);
        }
    }

    public CameraData GetCameraData(CAMERA_TYPE cameraType)
    {
        return _cachedCameras.ContainsKey(cameraType) ? _cachedCameras[cameraType] : null;
    }

    [Serializable]
    public class CameraData
    {
        public CAMERA_TYPE CameraType;
        public Camera Camera;
        [HideInInspector] public Bounds Bounds;
    }
}

