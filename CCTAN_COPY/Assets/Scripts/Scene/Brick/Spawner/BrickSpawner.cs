﻿using System;
using System.Collections.Generic;
using UnityEngine;
using CAMERA_TYPE = CameraManager.CAMERA_TYPE;
using POOL_TYPE = ObjectPool.POOL_TYPE;

public class BrickSpawner : Singleton<BrickSpawner>, IPlayObserver
{
    [Space]
    [SerializeField] private BrickHudUISpawner _brickHudUiSpawner;
    [SerializeField] private Transform _target;

    public BrickHudUISpawner BrickHudUiSpawner { get { return _brickHudUiSpawner; } }
    public Transform Target { get { return _target; } }

    public bool IsPause { get; private set; }

    private List<BaseBrick> _bricks = new List<BaseBrick>();
    private List<BaseBrick> _items = new List<BaseBrick>();
    private Queue<CombineBrickData> _combineBrickDatas = new Queue<CombineBrickData>();
    private Bounds _bounds;

    #region Unity

    void Awake()
    {
        PlayManager.Instance.PlayUpdate += BrickSpawnerUpdate;
        PlayManager.Instance.AddObserver(this);
    }

    void Start()
    {
        _bounds = CameraManager.Instance.GetCameraData(CAMERA_TYPE.MAIN).Bounds;
    }

    void BrickSpawnerUpdate()
    {
        if (_combineBrickDatas.Count <= 0)
            return;

        StartCombine();
    }

    private void StartCombine()
    {
        var combineBrickData = _combineBrickDatas.Dequeue();

        var a = (ICombinable)combineBrickData.a;
        var b = (ICombinable)combineBrickData.b;

        if (a.CanCombine() == false || b.CanCombine() == false)
            return;

        StartCoroutine(a.ComineWith(combineBrickData.b));
    }

    #endregion

    private void Init()
    {
        InitBrick();
        InitItem();

    }
    public Vector3 GetDirectionToTarget(Vector3 pos)
    {
        return (Target.position - pos).normalized;
    }

    public void Pause()
    {
        IsPause = true;

        for (var i = 0; i < _bricks.Count; i++)
        {
            if (_bricks[i] is IPausable)
                ((IPausable)_bricks[i]).Pause();
        }
    }

    public void Resume()
    {
        IsPause = false;

        for (var i = 0; i < _bricks.Count; i++)
        {
            if (_bricks[i] is IPausable)
                ((IPausable)_bricks[i]).Resume();
        }
    }

    #region Item

    private BaseBrickData CreateItemData()
    {
        return new ItemData(_bounds, PlayManager.Instance.PlayTime);
    }

    private void InitItem()
    {
        CancelInvoke("SpawnItem");

        for (var i = _items.Count - 1; i >= 0; i--)
        {
            DestroyItem(_items[i], false);
        }
    }

    private void SpawnItem()
    {
        var brick = ObjectPools.Instance.PopItem(POOL_TYPE.ITEM).GetComponent<BaseBrick>();
        brick.InitBrick(CreateItemData());

        _items.Add(brick);
    }

    public void DestroyItem(BaseBrick brick, bool isGetItem)
    {
        if (isGetItem)
        {
            EffectManager.Instance.PlayBrickDestroyEffect(brick.transform.position);
            GetItem((ItemData)brick.BrickData);
        }

        BrickHudUISpawner.Instance.DestroyBrickHudUI(brick.BrickHudUI.gameObject);
        _items.Remove(brick);

        ObjectPools.Instance.PushItem(POOL_TYPE.ITEM, brick.gameObject);
    }

    private void GetItem(ItemData itemData)
    {
        // TODO : Play Get Item Effect Here
        ItemManager.Instance.GetItem(itemData);
    }

    #endregion

    #region Brick

    private BaseBrickData CreatBrickData()
    {
        return new BrickData(_bounds, PlayManager.Instance.PlayTime);
    }

    private void InitBrick()
    {
        CancelInvoke("SpawnBrick");

        for (var i = _bricks.Count - 1; i >= 0; i--)
        {
            DestroyBrick(_bricks[i], false);
        }

        _combineBrickDatas.Clear();
    }

    private void SpawnBrick()
    {
        var brick = ObjectPools.Instance.PopItem(POOL_TYPE.BRICK).GetComponent<BaseBrick>();
        brick.InitBrick(CreatBrickData());

        _bricks.Add(brick);
    }

    public void DestroyBrick(BaseBrick brick, bool isGetScore)
    {
        if (isGetScore)
        {
            EffectManager.Instance.PlayBrickDestroyEffect(brick.transform.position);
            PlayManager.Instance.AddScore();
        }

        BrickHudUiSpawner.DestroyBrickHudUI(brick.BrickHudUI.gameObject);
        _bricks.Remove(brick);

        ObjectPools.Instance.PushItem(POOL_TYPE.BRICK, brick.gameObject);
    }

    #endregion

    #region Combine

    public void CombineBrick(BaseBrick a, BaseBrick b)
    {
        var tempA = (ICombinable)a;
        var tempB = (ICombinable)b;

        if (tempA.IsCombining || tempB.IsCombining || a.Hp == 0 || b.Hp == 0)
            return;

        _combineBrickDatas.Enqueue(new CombineBrickData(a, b));
    }

    public class CombineBrickData
    {
        public BaseBrick a;
        public BaseBrick b;

        public CombineBrickData(BaseBrick a, BaseBrick b)
        {
            this.a = a;
            this.b = b;
        }
    }

    #endregion

    #region IPlayObserver

    public void OnNotify(PlayManager.PLAY_STATE playState)
    {
        switch (playState)
        {
            case PlayManager.PLAY_STATE.IDLE:
                Init();
                break;
            case PlayManager.PLAY_STATE.BEGIN:
                InvokeRepeating("SpawnBrick", ConstManager.Instance.GetConst(CONST_TYPE.BRICK_SPAWN_RATE), ConstManager.Instance.GetConst(CONST_TYPE.BRICK_SPAWN_RATE));
                InvokeRepeating("SpawnItem", ConstManager.Instance.GetConst(CONST_TYPE.ITEM_SPAWN_RATE), ConstManager.Instance.GetConst(CONST_TYPE.ITEM_SPAWN_RATE));
                break;
            case PlayManager.PLAY_STATE.PLAYING:
                if (PlayManager.Instance.IsFirstGame == false)
                    DestoryBrickNearByLauncher();
                break;
            case PlayManager.PLAY_STATE.END:
                CancelInvoke("SpawnBrick");
                CancelInvoke("SpawnItem");
                break;
            default:
                throw new ArgumentOutOfRangeException("playState", playState, null);
        }
    }

    private void DestoryBrickNearByLauncher()
    {
        var maximumDistance = ConstManager.Instance.GetConst(CONST_TYPE.CONTINUE_MAXIMUM_DISTANCE);

        for (var i = _bricks.Count - 1; i >= 0; i--)
        {
            var distance = LauncherManager.Instance.GetDistanceWithCenter(_bricks[i].transform);
            if (distance > maximumDistance)
                continue;

            DestroyBrick(_bricks[i], false);
        }
    }

    #endregion
}