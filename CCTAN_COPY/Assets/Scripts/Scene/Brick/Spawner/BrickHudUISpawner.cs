﻿using UnityEngine;
using POOL_TYPE = ObjectPool.POOL_TYPE;

public class BrickHudUISpawner : Singleton<BrickHudUISpawner>
{
    public BrickHudUI GetBrickHudUI()
    {
        var brickHudUI = ObjectPools.Instance.PopItem(POOL_TYPE.BRICK_HUD_UI).GetComponent<BrickHudUI>();
        brickHudUI.gameObject.SetActive(true);
        return brickHudUI;
    }

    public void DestroyBrickHudUI(GameObject brickHudUI)
    {
        ObjectPools.Instance.PushItem(POOL_TYPE.BRICK_HUD_UI, brickHudUI);
    }
}
