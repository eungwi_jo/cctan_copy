﻿using UnityEngine;

public class BrickData : BaseBrickData
{
    private static readonly Vector2[] SqurePoints = {
        new Vector2(-0.3f, 0.3f),
        new Vector2(0.3f, 0.3f),
        new Vector2(0.3f, -0.3f),
        new Vector2(-0.3f, -0.3f),
        new Vector2(-0.3f, 0.3f)
    };

    private static readonly Vector2[] TrianglePoints = {
        new Vector2(-0.3f, 0.34f),
        new Vector2(0.34f, -0.3f),
        new Vector2(-0.3f, -0.3f),
        new Vector2(-0.3f, 0.3f)
    };

    public Vector2[] Points;

    public BrickData(Bounds bounds, float time)
    {
        Shape = Random.Range(0, 5) < 4 ? BaseBrickData.SHAPE.SQUARE : BaseBrickData.SHAPE.TRIANGLE;
        Pos = JMath.RandomBetween(bounds.extents, bounds.extents * 1.1f);
        Points = GetPoints();

        Randomize(time);
    }

    protected override Color GetColor()
    {
        return Random.ColorHSV(0, 1f, 1f, 1f, 1f, 1f);
    }

    private Vector2[] GetPoints()
    {
        switch (Shape)
        {
            case SHAPE.SQUARE:
                return SqurePoints;
            case SHAPE.TRIANGLE:
                return TrianglePoints;
            default:
                return SqurePoints;
        }
    }
}
