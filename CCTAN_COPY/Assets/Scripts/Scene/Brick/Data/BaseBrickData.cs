﻿using UnityEngine;
using Random = UnityEngine.Random;

public abstract class BaseBrickData
{
    public enum SHAPE
    {
        SQUARE,
        TRIANGLE,
        CIRCLE
    }

    public SHAPE Shape;
    public int Hp;
    public Vector3 Pos;
    public Vector3 Rotation;
    public Color Color;

    protected void Randomize(float time)
    {
        var timeFactor = Mathf.CeilToInt(time / 20f);
        Hp = Random.Range(1 * timeFactor, 10 * timeFactor);
        Rotation = RandomRotation();
        Color = GetColor();
    }

    protected abstract Color GetColor();

    private Vector3 RandomRotation()
    {
        switch (Shape)
        {
            case SHAPE.SQUARE:
                return Vector3.zero;
            case SHAPE.TRIANGLE:
                return new Vector3(0, 0, 90 * Random.Range(0, 4));
            default:
                return Vector3.zero;
        }
    }

}