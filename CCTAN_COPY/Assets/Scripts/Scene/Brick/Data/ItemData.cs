﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class ItemData : BaseBrickData
{
    public enum ITEM_TYPE
    {
        LEVEL_UP,
        LAZER,
        BACK,
        STOP,
        BOMB,
    }

    public ITEM_TYPE ItemType;
    public float Duration;

    public ItemData(Bounds bounds, float time)
    {
        Shape = SHAPE.CIRCLE;
        Pos = JMath.RandomBetween(bounds.extents * .7f, bounds.extents * .9f);
        ItemType = (ITEM_TYPE)Random.Range(0, 5);
        Duration = 5f;

        Randomize(time);
    }

    protected override Color GetColor()
    {
        switch (ItemType)
        {
            case ITEM_TYPE.LEVEL_UP:
                return new Color32(53, 212, 234, 255);
            case ITEM_TYPE.LAZER:
                return new Color32(255, 0, 255, 255);
            case ITEM_TYPE.BACK:
                return new Color32(3, 202, 137, 255);
            case ITEM_TYPE.STOP:
                return new Color32(219, 144, 79, 255);
            case ITEM_TYPE.BOMB:
                return new Color32(217, 4, 0, 255);
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}