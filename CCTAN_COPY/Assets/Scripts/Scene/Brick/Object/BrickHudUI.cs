﻿using System;
using UnityEngine;
using CAMERA_TYPE = CameraManager.CAMERA_TYPE;

public class BrickHudUI : MonoBehaviour
{
    private static readonly double TOLERANCE = 1;

    [SerializeField] private CustomUIFollowTarget _followTarget;
    [SerializeField] private UILabel _hpLabel;
    [SerializeField] private ColorBlink _colorBlink;

    private BaseBrick _brick;
    public void SetTarget(BaseBrick target)
    {
        _brick = target;
        InitHp(_brick.Hp);

        _followTarget.gameCamera = CameraManager.Instance.GetCameraData(CAMERA_TYPE.MAIN).Camera;
        _followTarget.uiCamera = CameraManager.Instance.GetCameraData(CAMERA_TYPE.UI).Camera;
        _followTarget.target = _brick.transform;
        _followTarget.Offset = GetOffset(target.BrickData);

        _followTarget.Follow();
    }

    public void InitHp(int hp)
    {
        _hpLabel.text = hp.ToString();
        _hpLabel.color = _brick.Color;
    }

    public void SetHp(int hp)
    {
        InitHp(hp);
        _colorBlink.Play(_brick.Color);
    }

    private Vector3 GetOffset(BaseBrickData brickData)
    {
        if (brickData is ItemData)
        {
            return new Vector3(0, -25, 0);
        }

        switch (_brick.Shape)
        {
            case BaseBrickData.SHAPE.SQUARE:
            case BaseBrickData.SHAPE.CIRCLE:
                return Vector3.zero;
            case BaseBrickData.SHAPE.TRIANGLE:
                var eulerAngle = _brick.transform.eulerAngles;
                if (Math.Abs(eulerAngle.z) < TOLERANCE)
                {
                    return new Vector3(-20, -20, 0);
                }
                else if (Math.Abs(eulerAngle.z - 90) < TOLERANCE)
                {
                    return new Vector3(20, -20, 0);
                }
                else if (Math.Abs(eulerAngle.z - 180f) < TOLERANCE)
                {
                    return new Vector3(20, 20, 0);
                }
                else if (Math.Abs(eulerAngle.z - 270) < TOLERANCE)
                {
                    return new Vector3(-20, 20, 0);
                }
                else
                {
                    return Vector3.zero;
                }
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
