﻿using System.Collections;
using UnityEngine;

public class NormalBrick : BaseBrick, IPausable, IMoveable, ICombinable
{
    [SerializeField] private PolygonCollider2D _collider2D;

    public Vector3 CachedVelocity { get; private set; }
    public bool IsCombining { get; set; }

    public override void InitBrick(BaseBrickData brickData)
    {
        base.InitBrick(brickData);

        IsCombining = false;
        _collider2D.points = ((BrickData)brickData).Points;

        var direction = BrickSpawner.Instance.GetDirectionToTarget(gameObject.transform.position).normalized;
        SetVelocity(direction * ConstManager.Instance.GetConst(CONST_TYPE.BRICK_SPEED));

        if (BrickSpawner.Instance.IsPause)
            Pause();
    }

    protected override void DestroyBrick()
    {
        BrickSpawner.Instance.DestroyBrick(this, true);
    }

    #region Collision

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Bomb"))
        {
            DestroyBrick();
        }
        else if (collision.transform.CompareTag("Bullet"))
        {
            if (IsCombining)
                return;

            var contacts = collision.contacts;
            if (contacts == null || contacts.Length == 0)
                return;

            var point = Vector2.zero;
            point.x = contacts[0].normal.x;
            point.y = contacts[0].normal.y;

            var rigidBody = collision.transform.GetComponent<Rigidbody2D>();
            var velocity = rigidBody.velocity;
            rigidBody.velocity = JMath.RotateVector2(-velocity, 2f * JMath.GetDegree(velocity.normalized, point));

            HitBrick();
        }
        else if (collision.transform.CompareTag("Launcher"))
        {
            PlayManager.Instance.Die();
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Bomb"))
        {
            DestroyBrick();
        }
        else if (collision.transform.CompareTag("Brick"))
        {
            var distance = Vector3.Distance(collision.transform.position, gameObject.transform.position);
            if (distance < ConstManager.Instance.GetConst(CONST_TYPE.COMBINE_MINIMUM_DISTANCE))
            {
                BrickSpawner.Instance.CombineBrick(this, collision.transform.GetComponent<BaseBrick>());
            }
        } else if (collision.transform.CompareTag("Launcher"))
        {
            PlayManager.Instance.Die();
        }
    }

    #endregion

    #region IMoveable

    public void SetVelocity(Vector2 velocity)
    {
        _rigidbody2D.velocity = velocity;
        CachedVelocity = _rigidbody2D.velocity;
    }

    #endregion

    #region IPauseable

    public void Pause()
    {
        _rigidbody2D.velocity = Vector2.zero;
    }

    public void Resume()
    {
        _rigidbody2D.velocity = CachedVelocity;
    }

    #endregion

    #region ICombinable

    public bool CanCombine()
    {
        if (gameObject.activeSelf == false || Hp <= 0 || IsCombining)
            return false;

        return true;
    }

    public IEnumerator ComineWith(BaseBrick target)
    {
        IsCombining = true;
        ((ICombinable)target).IsCombining = true;

        var hp = Hp + target.Hp;
        var center = JMath.Center(gameObject.transform.position, target.transform.position);

        StartCoroutine(MoveToCombine(center));
        yield return StartCoroutine(((ICombinable)target).MoveToCombine(center));

        var direction = BrickSpawner.Instance.GetDirectionToTarget(gameObject.transform.position).normalized;
        SetVelocity(direction * ConstManager.Instance.GetConst(CONST_TYPE.BRICK_SPEED));

        InitHp(hp);
        BrickHudUI.InitHp(hp);

        IsCombining = false;
        ((ICombinable)target).IsCombining = false;

        BrickSpawner.Instance.DestroyBrick(target, false);

        if (BrickSpawner.Instance.IsPause)
            Pause();
    }

    public IEnumerator MoveToCombine(Vector3 moveTo)
    {
        var time = 0f;
        var speed = .1f;
        var pos = gameObject.transform.position;

        while (time <= speed)
        {
            time += Time.smoothDeltaTime;
            gameObject.transform.position = Vector3.Lerp(pos, moveTo, time / speed);
            yield return null;
        }
    }
    #endregion
}
