﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBrick : BaseBrick
{
    [SerializeField] private SpriteRenderer _iconRenderer;

    public override void InitBrick(BaseBrickData brickData)
    {
        base.InitBrick(brickData);

        var itemType = ((ItemData)brickData).ItemType;
        _iconRenderer.sprite = ResourceManager.Instance.GetSprite(ResourceManager.SPRITE_TYPE.ITEM_ICON, string.Format("ItemIcon_{0}", (int)itemType));

        _iconRenderer.color = Color;
    }

    protected override void DestroyBrick()
    {
        BrickSpawner.Instance.DestroyItem(this, true);
    }

    #region Collision

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Bomb"))
        {
            DestroyBrick();
        }
        else if (collision.transform.CompareTag("Bullet"))
        {
            var contacts = collision.contacts;
            if (contacts == null || contacts.Length == 0)
                return;

            var point = Vector2.zero;
            point.x = contacts[0].normal.x;
            point.y = contacts[0].normal.y;

            var rigidBody = collision.transform.GetComponent<Rigidbody2D>();
            var velocity = rigidBody.velocity;
            rigidBody.velocity = JMath.RotateVector2(-velocity, 2f * JMath.GetDegree(velocity.normalized, point));

            HitBrick();
        }
        else if (collision.transform.CompareTag("Launcher"))
        {
            PlayManager.Instance.Die();
        }
    }

    #endregion

}