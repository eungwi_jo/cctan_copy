﻿using System.Collections;
using UnityEngine;

public interface IPausable
{
    void Pause();
    void Resume();
}

public interface IMoveable
{
    Vector3 CachedVelocity { get; }
    void SetVelocity(Vector2 velocity);
}

public interface ICombinable
{
    bool IsCombining { get; set; }
    bool CanCombine();
    IEnumerator ComineWith(BaseBrick target);
    IEnumerator MoveToCombine(Vector3 moveTo);
}
