﻿using UnityEngine;

public class Wall : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Bullet"))
        {
            LauncherManager.Instance.DestroyBullet(collision.gameObject);
        }
    }
}
