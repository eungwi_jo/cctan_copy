﻿using System;
using UnityEngine;
using CAMERA_TYPE = CameraManager.CAMERA_TYPE;

public abstract class BaseBrick : MonoBehaviour
{
    [SerializeField] protected Rigidbody2D _rigidbody2D;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private ColorBlink _colorBlink;

    public BaseBrickData BrickData { get; protected set; }
    public int Hp { get; private set; }
    public BrickHudUI BrickHudUI { get; private set; }

    public BaseBrickData.SHAPE Shape { get { return BrickData.Shape; } }
    public Color Color { get { return BrickData.Color; } }

    public virtual void InitBrick(BaseBrickData brickData)
    {
        gameObject.SetActive(true);

        BrickData = brickData;
        _spriteRenderer.color = Color;

        gameObject.transform.position = brickData.Pos;
        gameObject.transform.rotation = Quaternion.Euler(brickData.Rotation);
        InitHp(brickData.Hp);

        BrickHudUI = BrickSpawner.Instance.BrickHudUiSpawner.GetBrickHudUI();
        BrickHudUI.SetTarget(this);

        _spriteRenderer.sprite = ResourceManager.Instance.GetSprite(ResourceManager.SPRITE_TYPE.BRICK, string.Format("Brick_{0}", (int)brickData.Shape));
    }

    protected void InitHp(int hp)
    {
        Hp = hp;
    }

    private void SetHp(int hp)
    {
        InitHp(hp);
        _colorBlink.Play(Color);

        BrickHudUI.SetHp(hp);
    }

    protected abstract void DestroyBrick();

    public void HitBrick()
    {
        SetHp(Hp - 1);

        if (Hp <= 0)
        {
            DestroyBrick();
        }
    }
}
