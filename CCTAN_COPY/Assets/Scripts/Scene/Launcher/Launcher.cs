﻿using UnityEngine;

public abstract class Launcher
{
    protected Transform _muzzle;

    public Launcher(Transform muzzle)
    {
        _muzzle = muzzle;
        Init();
    }

    public abstract void Init();
    public abstract void LauncherUpdate();
    protected abstract void Fire();
}
