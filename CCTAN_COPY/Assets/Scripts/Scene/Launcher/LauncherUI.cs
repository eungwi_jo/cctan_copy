﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using POOL_TYPE = ObjectPool.POOL_TYPE;

public class LauncherUI : Singleton<LauncherUI>, ILevelObserver
{
    [SerializeField] private UILabel _levelLabel;
    [SerializeField] private UIGrid _buffRoot;

    private List<ItemBuff> _itemBuffs = new List<ItemBuff>();

    void Awake()
    {
        PlayManager.Instance.AddObserver(this);
    }

    public void OnNotify(int level)
    {
        _levelLabel.text = string.Format("Lv. {0}", level);
    }

    public ItemBuff AddItemBuff(ItemManager.ItemInstance instance)
    {
        var itemBuff = ObjectPools.Instance.PopItem(POOL_TYPE.ITEM_BUFF).GetComponent<ItemBuff>();
        itemBuff.Init(instance);
        itemBuff.gameObject.SetActive(true);

        _itemBuffs.Add(itemBuff);
        _buffRoot.Reposition();

        return itemBuff;
    }

    public void RemoveItemBuff(ItemBuff itemBuff)
    {
        _itemBuffs.Remove(itemBuff);
        ObjectPools.Instance.PushItem(POOL_TYPE.ITEM_BUFF, itemBuff.gameObject);

    }
}
