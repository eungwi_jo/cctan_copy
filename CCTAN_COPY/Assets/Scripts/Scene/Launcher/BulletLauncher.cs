﻿using System.Collections.Generic;
using UnityEngine;
using POOL_TYPE = ObjectPool.POOL_TYPE;
using BULLET_TYPE = Bullet.BULLET_TYPE;

public class BulletLauncher : Launcher
{
    private float _speedUp = 1f;
    private float _cooldown;
    private BULLET_TYPE _bulletType;

    private List<GameObject> _bullets = new List<GameObject>();

    public BulletLauncher(Transform muzzle) : base(muzzle)
    {
        _bulletType = BULLET_TYPE.NORMAL;
    }

    public BulletLauncher(Transform muzzle, BULLET_TYPE bulletType) : base(muzzle)
    {
        _bulletType = bulletType;
    }

    public override void Init()
    {
        _cooldown = 0;
        UpdateSpeed();

        for (var i = _bullets.Count - 1; i >= 0; i--)
        {
            DestroyBullet(_bullets[i]);
        }
    }

    public override void LauncherUpdate()
    {
        _cooldown -= Time.smoothDeltaTime;
        if (_cooldown > 0)
            return;

        Fire();
    }

    protected override void Fire()
    {
        _cooldown = GetCooldown();

        var bullet = ObjectPools.Instance.PopItem(POOL_TYPE.BULLET);
        bullet.GetComponent<Bullet>().Init(_muzzle, _bulletType);

        _bullets.Add(bullet);
    }

    public void UpdateSpeed()
    {
        _speedUp = 1f - (PlayManager.Instance.Level - 1) * 0.1f;
    }

    private float GetCooldown()
    {
        var cooldown = Mathf.Clamp(ConstManager.Instance.GetConst(CONST_TYPE.BULLET_COOLDOWN) * _speedUp, 0.02f, ConstManager.Instance.GetConst(CONST_TYPE.BULLET_COOLDOWN));
        return cooldown;
    }

    public void DestroyBullet(GameObject bullet)
    {
        _bullets.Remove(bullet);

        ObjectPools.Instance.PushItem(POOL_TYPE.BULLET, bullet);
    }
}