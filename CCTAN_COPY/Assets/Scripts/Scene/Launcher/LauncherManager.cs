﻿using UnityEngine;
using BULLET_TYPE = Bullet.BULLET_TYPE;

public class LauncherManager : Singleton<LauncherManager>, IPlayObserver, ILevelObserver
{
    [SerializeField] private Transform _center;
    [SerializeField] private Transform _frontMuzzle;
    [SerializeField] private Transform _backMuzzle;

    [SerializeField] private GameObject _lazerEffect;
    [SerializeField] private GameObject _backSprite;

    private BulletLauncher _frontLauncher;
    private BulletLauncher _backLauncher;

    private Launcher _currentLauncher;

    #region Unity

    void Awake()
    {
        _frontLauncher = new BulletLauncher(_frontMuzzle, BULLET_TYPE.NORMAL);

        PlayManager.Instance.AddObserver((IPlayObserver)this);
        PlayManager.Instance.AddObserver((ILevelObserver)this);

        PlayManager.Instance.PlayUpdate += LauncherUpdate;
    }

    void LauncherUpdate()
    {
        if (_currentLauncher != null)
            _currentLauncher.LauncherUpdate();

        if (_backLauncher != null)
            _backLauncher.LauncherUpdate();
    }

    #endregion

    private void Init()
    {
        gameObject.transform.rotation = Quaternion.Euler(0, 0, 180);

        _frontLauncher.Init();

        UnsetBack();
        UnsetLazer();
    }

    private void UpdateSpeed()
    {
        _frontLauncher.UpdateSpeed();

        if (_backLauncher != null)
            _backLauncher.UpdateSpeed();
    }

    public void DestroyBullet(GameObject bullet)
    {
        _frontLauncher.DestroyBullet(bullet);
    }

    #region Launcher

    public void SetLazer()
    {
        _currentLauncher = new LazerLauncher(_frontMuzzle);
        _lazerEffect.SetActive(true);
    }

    public void UnsetLazer()
    {
        _currentLauncher = _frontLauncher;
        _lazerEffect.SetActive(false);
    }

    public void SetBack()
    {
        _backLauncher = new BulletLauncher(_backMuzzle, BULLET_TYPE.SMALL);
        _backSprite.SetActive(true);
    }

    public void UnsetBack()
    {
        _backLauncher = null;
        _backSprite.SetActive(false);
    }

    public float GetDistanceWithCenter(Transform target)
    {
        return Vector3.Distance(_center.position, target.position);
    }
    #endregion

    #region IPlayObserver

    public void OnNotify(PlayManager.PLAY_STATE playState)
    {
        switch (playState)
        {
            case PlayManager.PLAY_STATE.IDLE:
                Init();
                break;
            case PlayManager.PLAY_STATE.BEGIN:
                break;
            case PlayManager.PLAY_STATE.PLAYING:
                break;
            case PlayManager.PLAY_STATE.END:
                break;
        }
    }

    #endregion

    #region ILevelObserver

    public void OnNotify(int level)
    {
        UpdateSpeed();
    }

    #endregion
}
