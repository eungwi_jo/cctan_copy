﻿using UnityEngine;

public class LazerLauncher : Launcher
{
    private float _cooldown;

    public LazerLauncher(Transform muzzle) : base(muzzle) { }

    public override void Init()
    {
        _cooldown = 0;
    }

    public override void LauncherUpdate()
    {
        _cooldown -= Time.smoothDeltaTime;
        if (_cooldown > 0)
            return;

        Fire();
    }

    protected override void Fire()
    {
        _cooldown = ConstManager.Instance.GetConst(CONST_TYPE.LAZER_COOLDOWN);

        var hits = Physics2D.CircleCastAll(_muzzle.position, ConstManager.Instance.GetConst(CONST_TYPE.LAZER_RADIUS), _muzzle.up, ConstManager.Instance.GetConst(CONST_TYPE.LAZER_DISTANCE));
        for (var i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.CompareTag("Brick") || hits[i].transform.CompareTag("Item"))
            {
                hits[i].transform.GetComponent<BaseBrick>().HitBrick();
            }
        }
    }
}
