﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public enum BULLET_TYPE
    {
        NORMAL,
        SMALL
    }

    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private CircleCollider2D _collider;
    [SerializeField] private Rigidbody2D _rigidbody;

    public void Init(Transform muzzle, BULLET_TYPE bulleType)
    {
        gameObject.SetActive(true);
        gameObject.transform.position = muzzle.transform.position;
        gameObject.transform.rotation = muzzle.rotation;

        _rigidbody.velocity = muzzle.up * ConstManager.Instance.GetConst(CONST_TYPE.BULLET_SPEED);
        _spriteRenderer.sprite = ResourceManager.Instance.GetSprite(ResourceManager.SPRITE_TYPE.BULLET, string.Format("Bullet_{0}", (int)bulleType));

        switch (bulleType)
        {
            case BULLET_TYPE.NORMAL:
                _collider.radius = .1f;
                break;
            case BULLET_TYPE.SMALL:
                _collider.radius = .05f;
                break;
        }
    }
}