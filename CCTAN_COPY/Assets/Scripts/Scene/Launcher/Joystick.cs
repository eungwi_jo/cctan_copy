﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joystick : MonoBehaviour, IPlayObserver
{
    [SerializeField] private GameObject _bulletLauncher;
    [SerializeField] private float _radius;

    private Camera _camera;
    private Vector3 _center;

    void Awake()
    {
        _center = gameObject.transform.position;

        PlayManager.Instance.PlayUpdate += JoyStickUpdate;
        PlayManager.Instance.AddObserver(this);

        ResetJoystick();
    }

    void Start()
    {
        _camera = CameraManager.Instance.GetCameraData(CameraManager.CAMERA_TYPE.UI).Camera;
    }

    private void ResetJoystick()
    {
        gameObject.transform.localPosition = Vector3.zero;
    }

    void JoyStickUpdate()
    {
        var mousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;

        var rad = Mathf.Atan2(mousePos.x - _center.x, mousePos.y - _center.y);

        var pos = Vector3.zero;
        pos.x = _center.x + _radius * Mathf.Sin(rad);
        pos.y = _center.y + _radius * Mathf.Cos(rad);
        pos.z = _center.z;

        gameObject.transform.position = pos;
        _bulletLauncher.transform.rotation = Quaternion.Euler(0, 0, -rad * Mathf.Rad2Deg);
    }

    public void OnNotify(PlayManager.PLAY_STATE playState)
    {
        switch (playState)
        {
            case PlayManager.PLAY_STATE.IDLE:
                ResetJoystick();
                break;
            case PlayManager.PLAY_STATE.BEGIN:
                break;
            case PlayManager.PLAY_STATE.PLAYING:
                break;
            case PlayManager.PLAY_STATE.END:
                ResetJoystick();
                break;
            default:
                throw new ArgumentOutOfRangeException("playState", playState, null);
        }
    }

}
