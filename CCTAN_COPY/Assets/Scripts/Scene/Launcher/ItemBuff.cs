﻿using System;
using UnityEngine;
using ITEM_TYPE = ItemData.ITEM_TYPE;
public class ItemBuff : MonoBehaviour
{
    [SerializeField] private UISprite _buffIcon;
    [SerializeField] private UILabel _remainTimeLabel;

    private ItemManager.ItemInstance _instance;

    public void Init(ItemManager.ItemInstance instance)
    {
        _instance = instance;
        _buffIcon.spriteName = string.Format("ItemIcon_{0}", (int)instance.ItemData.ItemType);

        var color = GetColor();
        _buffIcon.color = color;
        _remainTimeLabel.color = color;
    }

    public void UpdateUI()
    {
        _remainTimeLabel.text = Mathf.CeilToInt(_instance.RemainTime).ToString();
    }

    private Color GetColor()
    {
        switch (_instance.ItemData.ItemType)
        {
            case ITEM_TYPE.LEVEL_UP:
                return new Color32(53, 212, 234, 255);
            case ITEM_TYPE.LAZER:
                return new Color32(255, 0, 255, 255);
            case ITEM_TYPE.BACK:
                return new Color32(3, 202, 137, 255);
            case ITEM_TYPE.STOP:
                return new Color32(219, 144, 79, 255);
            case ITEM_TYPE.BOMB:
                return new Color32(217, 4, 0, 255);
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
