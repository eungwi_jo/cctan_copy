﻿using UnityEditor;

[CustomEditor(typeof(UIWidgetWave))]
public class UIWidgetWaveInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUILayout.Space();

        var widgetWave = (UIWidgetWave)target;

        widgetWave.UseTweenColor = EditorGUILayout.Toggle("Use Tween Color", widgetWave.UseTweenColor);
        if (widgetWave.UseTweenColor)
        {
            EditorGUI.indentLevel++;
            widgetWave.ColorTime = EditorGUILayout.FloatField("Color Time", widgetWave.ColorTime);
            widgetWave.FromColor = EditorGUILayout.ColorField("From Color", widgetWave.FromColor);
            widgetWave.ToColor = EditorGUILayout.ColorField("To Color", widgetWave.ToColor);
            widgetWave.ColorCurve = EditorGUILayout.CurveField("Color  Curve", widgetWave.ColorCurve);
            widgetWave.DisableAfterColor = EditorGUILayout.Toggle("Disable After Color", widgetWave.DisableAfterColor);

            EditorGUI.indentLevel--;
        }

        EditorGUILayout.Space();
        widgetWave.UseTweenScale = EditorGUILayout.Toggle("Use Tween Scale", widgetWave.UseTweenScale);
        if (widgetWave.UseTweenScale)
        {
            EditorGUI.indentLevel++;
            widgetWave.ScaleTime = EditorGUILayout.FloatField("Scale Time", widgetWave.ScaleTime);
            widgetWave.FromScale = EditorGUILayout.Vector3Field("From Scale", widgetWave.FromScale);
            widgetWave.ToScale = EditorGUILayout.Vector3Field("To Scale", widgetWave.ToScale);
            widgetWave.ScaleCurve = EditorGUILayout.CurveField("Scale Curve", widgetWave.ScaleCurve);
            widgetWave.DisableAfterScale = EditorGUILayout.Toggle("Disable After Scale", widgetWave.DisableAfterScale);

            EditorGUI.indentLevel--;
        }

        EditorGUILayout.Space();
        widgetWave.UseTweenPos = EditorGUILayout.Toggle("Use Tween Pos", widgetWave.UseTweenPos);
        if (widgetWave.UseTweenPos)
        {
            EditorGUI.indentLevel++;
            widgetWave.PosTime = EditorGUILayout.FloatField("Alpha Time", widgetWave.PosTime);
            widgetWave.FromPos = EditorGUILayout.Vector3Field("From Pos", widgetWave.FromPos);
            widgetWave.ToPos = EditorGUILayout.Vector3Field("To Pos", widgetWave.ToPos);
            widgetWave.PosCurve = EditorGUILayout.CurveField("Pos Curve", widgetWave.PosCurve);
            widgetWave.DisableAfterPos = EditorGUILayout.Toggle("Disable After Pos", widgetWave.DisableAfterPos);

            EditorGUI.indentLevel--;
        }

        if (!widgetWave.UseTweenColor && !widgetWave.UseTweenScale && !widgetWave.UseTweenPos)
        {
            EditorGUILayout.Space();
            EditorGUILayout.HelpBox("At least one Tween setting is required.", MessageType.Warning);
        }
    }
}
