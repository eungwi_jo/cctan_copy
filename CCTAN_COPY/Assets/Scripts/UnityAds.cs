﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAds : Singleton<UnityAds>
{
    private readonly string AndroidGameId = "2695352";
    private readonly string iOSGameId = "2695353";
    private readonly string RewardedVideoId = "rewardedVideo";

    void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
#if UNITY_ANDROID
        Advertisement.Initialize(AndroidGameId);
#elif UNITY_IPHONE
        Advertisement.Initialize(iOSGameId);
#endif
    }

    public void ShowAdForOneMore()
    {
        if (Advertisement.IsReady(RewardedVideoId) == false)
            return;

        var options = new ShowOptions();
        options.resultCallback = HandleShowAdForOneMore;

        Advertisement.Show(RewardedVideoId, options);
    }

    private void HandleShowAdForOneMore(ShowResult result)
    {
        Debug.Log(result.ToString());

        switch (result)
        {
            case ShowResult.Failed:
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Finished:
                PlayManager.Instance.OneMoreGame();
                break;
            default:
                throw new ArgumentOutOfRangeException("result", result, null);
        }
    }
}
