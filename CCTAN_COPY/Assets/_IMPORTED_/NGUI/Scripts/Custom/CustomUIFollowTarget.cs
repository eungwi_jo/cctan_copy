using UnityEngine;

public class CustomUIFollowTarget : UIFollowTarget
{
    [SerializeField] private bool _followWhenInvisible;
    [SerializeField] private bool _isFollowRotation;
    public Vector3 Offset;


    void Update()
    {
        Follow();
    }

    public void Follow()
    {
        if (target && uiCamera != null)
        {
            Vector3 pos = gameCamera.WorldToViewportPoint(target.position);

            // Determine the visibility and the target alpha
            int isVisible = (gameCamera.orthographic || pos.z > 0f) && (pos.x > 0f && pos.x < 1f && pos.y > 0f && pos.y < 1f) ? 1 : 0;
            bool vis = (isVisible == 1);

            // If visible, update the position
            if (_followWhenInvisible || vis)
            {
                pos = uiCamera.ViewportToWorldPoint(pos);
                pos = mTrans.parent.InverseTransformPoint(pos);
                pos += Offset;
                //pos.x = Mathf.RoundToInt(pos.x);
                //pos.y = Mathf.RoundToInt(pos.y);
                pos.z = 0f;
                mTrans.localPosition = pos;

                if (_isFollowRotation)
                {
                    mTrans.rotation = target.rotation;
                }
            }

            // Update the visibility flag
            if (mIsVisible != isVisible)
            {
                mIsVisible = isVisible;

                if (disableIfInvisible)
                {
                    for (int i = 0, imax = mTrans.childCount; i < imax; ++i)
                        NGUITools.SetActive(mTrans.GetChild(i).gameObject, vis);
                }

                if (onChange != null) onChange(vis);
            }
        }
        else Destroy(gameObject);
    }

}
