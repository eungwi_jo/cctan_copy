﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class WaveTest : MonoBehaviour
{
    public UIWidgetWave wave;
    public int score = 0;

    void Start()
    {
        ((UILabel)wave.TargetWidget).text = (++score).ToString("N0");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ((UILabel)wave.TargetWidget).text = (++score).ToString("N0");
            wave.Emit(1);
        }
    }
}
