﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorxTest : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;

    public Color[] a;
    public int index;
    [Range(0, 1)]
    public float t;

    void Update()
    {
        var i = Mathf.Clamp(index % 10, 0, a.Length - 1);
        spriteRenderer.color = Colorx.Slerp(a[i], a[i + 1], t);
    }
}
