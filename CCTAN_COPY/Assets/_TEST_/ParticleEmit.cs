﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEmit : MonoBehaviour
{
    public ParticleSystem ParticleSystem;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Space))
	    {
            ParticleSystem.Stop();
	        ParticleSystem.Clear();
            ParticleSystem.Play();
	    }

	    if (Input.GetKeyDown(KeyCode.DownArrow))
	    {
	        ParticleSystem.gameObject.transform.Translate(0, -0.5f, 0);
	    }
	    if (Input.GetKeyDown(KeyCode.UpArrow))
	    {
	        ParticleSystem.gameObject.transform.Translate(0, 0.5f, 0);
	    }
	    if (Input.GetKeyDown(KeyCode.LeftArrow))
	    {
	        ParticleSystem.gameObject.transform.Translate(-0.5f, 0, 0);
	    }
	    if (Input.GetKeyDown(KeyCode.RightArrow))
	    {
	        ParticleSystem.gameObject.transform.Translate(0.5f, 0, 0);
	    }
    }
}
